const path = require('path');

module.exports = function override(config, env) {
    if (!config.plugins) {
        config.plugins = [];
    }

    config.resolve = {
        alias: {
            '@images': path.resolve(__dirname, 'src/images'),
            '@components': path.resolve(__dirname, 'src/components'),
            '@helpers': path.resolve(__dirname, 'src/helpers')
        }
    };

    console.log('123456');

    return config;
};