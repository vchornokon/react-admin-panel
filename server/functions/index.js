const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();
exports.users = functions.https.onRequest((request, response) => {
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers,Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization");

    const {offset, limit} = request.body.data || {offset: 0, limit: 10};

    admin.database().ref('users').once('value').then(snapshot => {
        const keys = Object.keys(snapshot.val());
        return {startAt: keys[offset], allCount: keys.length};
    }).then(({startAt, allCount}) => {
        admin.database().ref('users').startAt(null, startAt).limitToFirst(limit).once('value').then(snapshot => {
            const users = [];
            snapshot.forEach(item => {
                const user = item.val();
                users.push({id: item.key, ...user});
            });
            return {data: users, allCount};
        }).then(data => {
            response.send({data});
        });
    });
});

exports.fakeUsers = functions.https.onRequest((request, response) => {
    const inserts = [];
    for (let i = 0; i < 10; i++) {
        inserts.push(admin.database().ref('users').push()
            .set({username: `test${i}@example.com`}));
    }
    Promise.all(inserts).then(() => {
        response.send('Users Created!');
    });
});