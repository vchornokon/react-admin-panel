import avatar from '@images/user2-160x160.jpg';

const data = {
    user: {
        name: 'Test User',
        avatar: avatar
    },
    users: [
        {id: 1, name: 'User 1', email: 'test@example.com', create_at: '30-11-2019 12:01:01', updated_at: '30-11-2019 14:01:01', discount: 15.50, last_login: '30-11-2019 14:01:01', status: 'active'},
        {id: 2, name: 'User 1', email: 'test2@example.com', create_at: '30-11-2019 12:01:01', updated_at: '30-11-2019 14:01:01', discount: 0, last_login: '30-11-2019 14:01:01', status: 'inactive'},
        {id: 3, name: 'User 3', email: 'test3@example.com', create_at: '30-11-2019 12:01:01', updated_at: '30-11-2019 14:01:01', discount: 99, last_login: '30-11-2019 14:01:01', status: 'active'},
        {id: 4, name: 'User 4', email: 'test4@example.com', create_at: '30-11-2019 12:01:01', updated_at: '30-11-2019 14:01:01', discount: 4, last_login: '30-11-2019 14:01:01', status: 'active'},
        {id: 5, name: 'User 5', email: 'test5@example.com', create_at: '30-11-2019 12:01:01', updated_at: '30-11-2019 14:01:01', discount: 0, last_login: '30-11-2019 14:01:01', status: 'active'},
    ]
};

export const  request = function(url, filters){
    return new Promise((resolve, reject) => {
        let response = data[url];

        switch (url) {
            case 'users':
                response = response.filter(item => item.name.includes(filters.search));
                response = {
                    data: response.slice(filters.page * 2, (filters.page + 1) * 2),
                    allCount: Math.round(response.length / 2)
                };
                break;
            case 'orders':
                response = response.filter(item => item.name.includes(filters.search));
                response = {
                    data: response.slice(filters.page * 2, (filters.page + 1) * 2),
                    allCount: Math.round(response.length / 2)
                };
                break;
            default:

        }
        setTimeout(() => resolve(response), 0);
    })
};