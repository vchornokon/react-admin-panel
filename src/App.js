import React, {useReducer} from 'react';
import './App.css';
import Dashboard from './pages/dashboard';
import {UserContext} from './contexts/user';
import withTheme from './contexts/theme';
import withFirebase from './contexts/firebase';
import {withNotificationContext} from './components/Notifications/notifications.context';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Login from './pages/login';
import usersReducer from "./pages/users/users.reducer";

function App() {
    console.log('App draw');
    const [user, dispatchUser] = useReducer(usersReducer, JSON.parse(localStorage.getItem('user')) || null);
    const isAuthenticated = !!user;
    const PrivateRoute = ({component, ...rest}) => {
        return <Route {...rest} render={props =>
            isAuthenticated
                ? (React.createElement(component, props))
                : (<Redirect to={{pathname: "/login", state: {from: props.location}}}/>)
        }/>
    };

    const PublicRoute = ({component, ...rest}) => {
        return <Route {...rest} render={props =>
            isAuthenticated
                ? (<Redirect to={{pathname: "/"}}/>)
                : React.createElement(component, props)
        }/>
    };

    return (
        <UserContext.Provider value={{user, dispatchUser}}>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" render={() => <Redirect to="/app/dashboard"/>}/>
                    <Route exact path="/app" render={() => <Redirect to="/app/dashboard"/>}/>
                    <PrivateRoute path="/app" component={Dashboard}/>
                    <PublicRoute path="/login" component={Login}/>
                </Switch>
            </BrowserRouter>
        </UserContext.Provider>
    );
}

export default withTheme(withFirebase(withNotificationContext(App)));