export const TYPE_USERS_SET = 'admin-panel/users/SET';

export default function(state, action){
    switch (action.type) {
        case TYPE_USERS_SET:
            return action.payload;
        default:
            return state;
    }

}