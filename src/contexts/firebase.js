import React, {createContext} from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/functions';


export const FirebaseContext  = createContext(null);

const withFirebase = OriginalComponent => {
    const config = {
        apiKey: process.env.REACT_APP_API_KEY,
        authDomain: process.env.REACT_APP_AUTH_DOMAIN,
        databaseURL: process.env.REACT_APP_DATABASE_URL,
        projectId: process.env.REACT_APP_PROJECT_ID,
        storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
        messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    };

    firebase.initializeApp(config);

    const db = firebase.database();

    if(process.env.REACT_APP_USE_FUNCTIONS_EMULATOR)
        firebase.functions().useFunctionsEmulator(process.env.REACT_APP_USE_FUNCTIONS_EMULATOR);
    const userModel = {
        user: uid => db.ref(`users/${uid}`),
        users: () => db.ref('users'),
        find: firebase.functions().httpsCallable('users')
    };

    return props =>
        <FirebaseContext.Provider value={{firebase, userModel}}>
            <OriginalComponent {...props}/>
        </FirebaseContext.Provider>
};

export default withFirebase;