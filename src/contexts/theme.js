import React from 'react';
import '../index.css';

import {createContext} from 'react';
import logo from '@images/AdminLTELogo.png';

export const ThemeContext  = createContext();

const withTheme = OriginalComponent => {
    const theme = {
        logo: logo
    };

    return (...props) =>
        <ThemeContext.Provider value={theme}>
            <OriginalComponent {...props}/>
        </ThemeContext.Provider>
};

export default withTheme;