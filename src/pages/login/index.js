import React, {useContext, useState} from 'react';
import {UserContext} from "../../contexts/user";
import {FirebaseContext} from "../../contexts/firebase";
import {useNotificationContext} from "@components/Notifications/notifications.context";
import {TYPE_USERS_SET} from "../../reducers/user.reducer";

const Login = () => {
    const {dispatchUser} = useContext(UserContext);
    const {firebase, userModel} = useContext(FirebaseContext);
    const notification = useNotificationContext();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const login = (event) => {
        event.preventDefault();

        const auth = firebase.auth();
        auth.signInWithEmailAndPassword(username, password).then(({user: authUser}) => {
            return userModel.user(authUser.uid).once('value')
                .then(snapshot => {
                    if (!snapshot.exists())
                        return null;

                    return snapshot.val();
                })
                .then(user => {
                    if(user)
                        return user;

                    return userModel.users().child(authUser.uid).set({
                        username: username
                    });
                })
                .then(user => user ? user : {username: username})
                .then(user => ({...user, uid: authUser.uid}))
                .then(user => {
                    localStorage.setItem('user', JSON.stringify(user));
                    dispatchUser({
                        type: TYPE_USERS_SET,
                        payload: user
                    });
                })
                .catch(err => {
                    console.log(err);
                    notification.add('danger', 'DB Error!');
                });
        }).catch(err => {
            notification.add('danger', err.message);
        });
    };

    return (
        <div className="login-page">
            <div className="login-box">
                <div className="login-logo">
                    <a href="/"><b>Admin</b>LTE</a>
                </div>
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Sign in to start your session</p>
                        <form action="/" method="post" onSubmit={event => login(event)}>
                            <div className="input-group mb-3">
                                <input type="text"
                                       className="form-control"
                                       placeholder="Email"
                                       name="username"
                                       value={username}
                                       onChange={(event) => setUsername(event.target.value)}/>
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope"/>
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Password"
                                    name="password"
                                    value={password}
                                    onChange={(event) => setPassword(event.target.value)}/>
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock"/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-8">
                                    <div className="icheck-primary">
                                        <input type="checkbox" id="remember"/>
                                        <label htmlFor="remember">
                                            Remember Me
                                        </label>
                                    </div>
                                </div>
                                <div className="col-4">
                                    <button type="submit" className="btn btn-primary btn-block">Sign In</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;