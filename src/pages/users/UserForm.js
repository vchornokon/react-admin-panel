import React, {useRef} from 'react';
// import PropTypes from 'prop-types';
import ContentHeader from "@components/common/ContentHeader";
import Card from "@components/common/Card";
import Button from "../../components/common/Form/Button";
import Form from "../../components/common/Form/Form";

const UserForm = () => {
    const breadcrumbs = [{
        title: 'Home',
        url: '/',
    }, {
        title: 'Users',
        isActive: true,
    }];

    const saveUserHandler = (event) => {
        event.preventDefault();
        formRef.current.click();
    };

    const formSchema = [
        {
            name: 'email',
            type: 'email',
            label: 'Email',
            validation: (val) => {
                if (!val)
                    return {isValid: false, message: 'This field is required'};

                if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(val))
                    return {isValid: false, message: 'This field must be an email'};

                return {isValid: true};
            }

        },
        {
            name: 'name',
            label: 'Name',
            validation: (val) => ({isValid: !!val, message: 'This field is required'})
        },
        {
            name: 'is_admin',
            label: 'Admin access',
            type: 'checkbox'
        },
        {
            name: 'image',
            label: 'File upload',
            type: 'dropzone',
            validation: (val) => ({isValid: !!val, message: 'This field is required'})
        }
    ];

    const submitHandler = ({formIsValid, data}) => {
        console.log(formIsValid, data);
    };

    const formRef = useRef(null);

    return (
        <>
            <ContentHeader breadcrumbs={breadcrumbs} title={'Create User'}/>
            <section className="content">
                <Card
                    title="Create User"
                    footer={<Button title="Save" onClick={(event) => saveUserHandler(event)}/>}
                >
                    <Form schema={formSchema} doSubmit={submitHandler} formRef={formRef}/>
                </Card>
            </section>
        </>
    );
};

UserForm.propTypes = {};

export default UserForm;