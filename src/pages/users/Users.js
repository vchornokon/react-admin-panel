import React, {useReducer, useContext} from 'react';
// import PropTypes from 'prop-types';
import ContentHeader from '@components/common/ContentHeader';
import usersReducer, {TYPE_USERS_SET} from "./users.reducer";
import {request} from "@helpers/fetch";
import {Button} from '@components/common/Form';
import Card from '@components/common/Card';
import Table from '@components/common/Table';
import {FirebaseContext} from "../../contexts/firebase";

const Users = () => {
    const breadcrumbs = [{
        title: 'Home',
        url: '/',
    }, {
        title: 'Users',
        isActive: true,
    }];

    const [users, dispatchUsers] = useReducer(usersReducer, []);
    const usersList = (users && users.data) ? users.data.map(item => {
        const buttons = <>
            <Button title="Show" size="xs" isOutline={true}/>
            <Button title="Edit" size="xs" type="secondary" isOutline={true}/>
            <Button title="Remove" size="xs" type="danger" isOutline={false}/>
        </>;
        return {...item, actions: buttons}
    }) : [];

    const header = (usersList[0]) ? Object.keys(usersList[0]) : [];

    const {userModel} = useContext(FirebaseContext);
    const limit = 5;

    const load = (filters) => {
        const offset = filters.page * limit;
        userModel.find({offset, limit}).then(result => {
            dispatchUsers({
                type: TYPE_USERS_SET,
                payload: result.data
            })
        });
    };

    const pages = Math.round((users.allCount || 0) / limit);

    return (
        <>
            <ContentHeader breadcrumbs={breadcrumbs} title={'Dashboard'}/>
            <section className="content">
                <Card
                    title="Users"
                    pagination={true}
                    search={true}
                    handleReload={load}
                    numberOfPages={pages}
                >
                    <Table items={usersList} header={header}/>
                </Card>
            </section>
        </>
    );
};

Users.propTypes = {

};

export default Users;