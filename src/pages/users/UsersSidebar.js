import React from 'react';
import {SidebarItem} from './../../components/Sidebar';

const urlPrefix = '/app/users';

export default () =>
    <SidebarItem
        title="Users"
        url={`${urlPrefix}/`}
        icon={React.createElement('i', {className: 'nav-icon fas fa-users'})}
    >
        <SidebarItem
            title="List"
            url={`${urlPrefix}/list`}
        />
        <SidebarItem
            title="Create"
            url={`${urlPrefix}/create`}
        />
        <SidebarItem
            title="Trash"
            url={`${urlPrefix}/trash`}
        />
    </SidebarItem>