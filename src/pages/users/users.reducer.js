export const TYPE_USERS_SET = 'admin-panel/users/SET';
export const TYPE_USERS_ADD = 'admin-panel/users/ADD';

export default function(state, action){
    console.log(action);
    switch (action.type) {
        case TYPE_USERS_SET:
            return action.payload;
        default:
            return state;
    }

}