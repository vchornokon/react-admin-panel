import React from 'react';
import Header from '@components/Header';
import Sidebar from '@components/Sidebar';
import Footer from '@components/Footer';
import {Switch, Route} from 'react-router-dom';
import Users from './../users/Users';
import UserForm from "../users/UserForm";

const Dashboard = () => {

    return (
        <div className="wrapper">
            <Header/>
            <Sidebar/>
            <div className="content-wrapper">
                <Switch>
                    <Route path="/app/dashboard"/>
                    <Route path="/app/users/list" component={Users} exact/>
                    <Route path="/app/users/create" component={UserForm}/>
                </Switch>
            </div>
            <Footer/>
            <aside className="control-sidebar control-sidebar-dark"></aside>
        </div>

    );
};


export default Dashboard;