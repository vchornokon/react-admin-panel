export const TYPE_NOTIFICATIONS_ADD = 'admin-panel/notifications/ADD';
export const TYPE_NOTIFICATIONS_CLEAR = 'admin-panel/notifications/CLEAR';
export const TYPE_NOTIFICATIONS_REMOVE = 'admin-panel/notifications/REMOVE';

export default function(state, action){
    switch (action.type) {
        case TYPE_NOTIFICATIONS_ADD:
            return [...state, action.payload];
        case TYPE_NOTIFICATIONS_REMOVE:
            return state.filter(item => item.uid !== action.payload);
        case TYPE_NOTIFICATIONS_CLEAR:
            return [];
        default:
            return state;
    }

}