import React from 'react';
import PropTypes from 'prop-types';
import {useNotificationContext} from './notifications.context';

const Notifications = ({notifications}) => {
    const notification = useNotificationContext();

    return (
        <div className="toasts-top-right fixed">
            {
                notifications.map((item, key) =>
                    <div className={`toast bg-${item.type || 'default'} fade show`} key={item.uid}>
                        <div className="toast-header">
                            <strong className="mr-auto">Toast Title</strong>
                            <button
                                onClick={() => notification.remove(item.uid)}
                                data-dismiss="toast" type="button" className="ml-2 mb-1 close"
                                    aria-label="Close"><span>×</span></button>
                        </div>
                        <div className="toast-body">{item.message}</div>
                    </div>)
            }
        </div>
    );
};

Notifications.propTypes = {
    'notifications': PropTypes.array
};

export default Notifications;