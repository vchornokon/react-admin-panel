import React, {createContext, useContext, useReducer} from 'react';
import reducer, {
    TYPE_NOTIFICATIONS_ADD,
    TYPE_NOTIFICATIONS_CLEAR,
    TYPE_NOTIFICATIONS_REMOVE
} from './notifications.reducer';
import Notifications from "./Notifications";
import uuid from 'uuid-random';

const Context = createContext();

export const useNotificationContext = () => {
    return useContext(Context);
};

export const withNotificationContext = (OriginalComponent) => props =>
    <NotificationsProvider>
        <OriginalComponent {...props}/>
    </NotificationsProvider>;

export const NotificationsProvider = ({children}) => {
    const [notifications, dispatch] = useReducer(reducer, []);
    const notification = {
        add: (type, message) => {
            const uid = uuid();
            dispatch({
                type: TYPE_NOTIFICATIONS_ADD,
                payload: {type, message, uid}
            });
            setTimeout(() => {
                dispatch({
                    type: TYPE_NOTIFICATIONS_REMOVE,
                    payload: uid
                });
            }, 3000);
        },
        remove: (uid = null) => {
            if (!uid) {
                dispatch({
                    type: TYPE_NOTIFICATIONS_CLEAR
                });
            } else {
                dispatch({
                    type: TYPE_NOTIFICATIONS_REMOVE,
                    payload: uid
                });
            }

        }
    };

    return (
        <Context.Provider value={notification}>
            {children}
            {
                <Notifications notifications={notifications}/>
            }
        </Context.Provider>
    );
};