import React from 'react';

const ContentHeader = function ({title, breadcrumbs}) {
    return (
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>{title}</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            {
                                breadcrumbs && breadcrumbs.map((item, id) =>
                                    <li key={id} className={`breadcrumb-item ${item.isActive && 'active'}`}>
                                        {
                                            ('undefined' !== typeof item.url) && item.url
                                                ? <a href={item.url}>{item.title}</a>
                                                : item.title
                                        }
                                    </li>
                                )
                            }
                        </ol>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ContentHeader;