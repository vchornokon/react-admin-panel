import React, {useReducer, useEffect} from 'react';
import PropTypes from 'prop-types';
import Search from './Search';
import {Context} from './card.context';
import Pagination from './Pagination';
import reducer, {initState} from './card.reducer';

const Card = ({title, children, numberOfPages, handleReload, search, pagination, footer}) => {
    const [state, dispatch] = useReducer(reducer, initState);

    useEffect(() =>{
        handleReload &&
            handleReload(state);
        // eslint-disable-next-line
    }, [state]);

    return (
        <Context.Provider value={{state, dispatch}}>
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title">{title}</h3>
                    <div className="card-tools">
                        {
                            search ? <Search/> : null
                        }
                    </div>
                </div>
                <div className="card-body">
                    {children}
                </div>
                <div className="card-footer clearfix">
                    {footer}
                    {
                        pagination ? <Pagination numberOfPages={numberOfPages}/> : null
                    }
                </div>
            </div>
        </Context.Provider>
    );
};

Card.propTypes = {
    title: PropTypes.string,
    headRight: PropTypes.element,
    pagination: PropTypes.bool,
    search: PropTypes.bool,
    // footer: PropTypes.element,
};

Card.defaultProps = {
    title: 'Card Title',
    headRight: null,
    pagination: false,
    search: false,
    footer: ''
};

export default Card;