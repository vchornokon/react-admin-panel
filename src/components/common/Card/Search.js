import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useCardContext} from './card.context';
import {TYPE_SEARCH_SET} from './card.reducer';

const Search = () => {
    const [search, setSearch] = useState('');
    const {dispatch} = useCardContext();

    const doSearch = () => {
        dispatch({
            type: TYPE_SEARCH_SET,
            payload: search
        });
    };

    return (
        <div className="input-group input-group-sm" style={{width: '350px'}}>
            <input type="text"
                   className="form-control float-right"
                   placeholder="Search"
                   value={search}
                   onChange={(event) => setSearch(event.target.value)}
            />
            <div className="input-group-append">
                <button
                    className="btn btn-default"
                    onClick={doSearch}
                >
                    <i className="fas fa-search"/>
                </button>
            </div>
        </div>
    );
};

Search.propTypes = {
    doSearch: PropTypes.func
};

export default Search;