import React from 'react';
import PropTypes from 'prop-types';
import {useCardContext} from './card.context';
import {TYPE_PAGE_SET} from "./card.reducer";

const Pagination = ({numberOfPages}) => {
    const {state, dispatch} = useCardContext();
    const page = state.page;


    const doPaginate = (event, toPage) => {
        event.preventDefault();
        dispatch({
            type: TYPE_PAGE_SET,
            payload: toPage
        });
    };

    return (
        <ul className="pagination pagination-sm m-0 float-right">
            <li className="page-item"><a className="page-link" href="/" onClick={(event) => doPaginate(event, 0)}>&laquo;</a></li>
            {
                Array.from(Array(numberOfPages).keys()).map(item =>
                    <li className={`page-item ${(page === item) && 'active'}`} key={item}>
                        <a className="page-link" onClick={(event) => doPaginate(event, item)} href="/">{item + 1}</a>
                    </li>)
            }
            <li className="page-item"><a className="page-link" href="/" onClick={(event) => doPaginate(event, numberOfPages-1)}>&raquo;</a></li>
        </ul>
    );
};

Pagination.propTypes = {
    numberOfPages: PropTypes.number
};

Pagination.defaultProps = {
    numberOfPages: 1
};

export default Pagination;