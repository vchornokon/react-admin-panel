export const TYPE_SEARCH_SET = 'admin-panel/card/SEARCH_SET';
export const TYPE_PAGE_SET = 'admin-panel/card/PAGE_SET';

export default (state, action) => {
    switch (action.type) {
        case TYPE_SEARCH_SET:
            return {
                ...state,
                search: action.payload,
                page: 0
            };
        case TYPE_PAGE_SET:
            return {
                ...state,
                page: action.payload
            };
        default:
            return state;
    }
};

export const initState = {
    search: '',
    page: 0
};