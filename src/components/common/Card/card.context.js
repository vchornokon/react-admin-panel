import React, {createContext, useContext} from 'react';

export const Context = createContext();

export const useCardContext = () => {
    return useContext(Context);
};

export const withCardContext = (handlers) =>
    (OriginalComponent) => {
        return (props) => (
            <Context.Provider value={handlers}>
                <OriginalComponent {...props}/>
            </Context.Provider>
        )
    };