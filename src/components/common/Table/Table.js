import React from 'react';
import PropTypes from 'prop-types';
import EmptyRow from "./EmptyRow";

const Table = function ({items, header}) {
    return (
            <table className="table table-bordered">
                <thead>
                <tr>
                    {
                        header.map(item => <th key={item}>{item}</th>)
                    }
                </tr>
                </thead>
                <tbody>
                {
                    (items && items.length) ?
                        items.map((item, key) =>{
                            return <tr key={key}>
                                {
                                    Object.keys(item).map(col => <td key={col}>{item[col]}</td>)
                                }
                            </tr>})
                        : <EmptyRow colSpan={header.length}/>
                }
                </tbody>
            </table>
    )
};

Table.propTypes = {
    items: PropTypes.array
};

Table.defaultProps = {
    items: [],
};

export default Table;