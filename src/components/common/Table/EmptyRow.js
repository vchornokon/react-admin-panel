import React from 'react';
import PropTypes from 'prop-types';

const EmptyRow = function({colSpan, message}){

    return <tr><td className="text-center" colSpan={colSpan}>{message}</td></tr>
};

EmptyRow.propTypes = {
    colSpan: PropTypes.number,
    message: PropTypes.string
};

EmptyRow.defaultProps = {
    colSpan: 1,
    message: 'Table is empty...'
};

export default EmptyRow;
