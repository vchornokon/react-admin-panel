import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import {Context} from './../contexts/context';
import {TYPE_USER_TOGGLE} from './../reducer';

function TableRow({id, name, isChecked}){
    const {dispatch} = useContext(Context);

    return (
        <tr>
            <td>
                <label>
                    <input
                        type="checkbox"
                        checked={isChecked}
                        onChange={() => dispatch({
                            type: TYPE_USER_TOGGLE,
                            payload: id
                        })}
                    />
                    <span> </span>
                </label>
            </td>
            <td>{id}</td>
            <td>{name}</td>
        </tr>
    );
}

TableRow.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    isChecked: PropTypes.bool
};

TableRow.defaultProps = {
    name: '',
    isChecked: false
};

export default TableRow;