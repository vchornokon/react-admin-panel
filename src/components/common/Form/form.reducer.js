export const TYPE_SET_FIELDS = 'admin-panel/form/SET_FIELDS';
export const TYPE_SET_VALUES = 'admin-panel/form/SET_VALUES';
export const TYPE_SET_FIELD_VALUE = 'admin-panel/form/SET_FIELD_VALUE';
export const TYPE_SET_FIELD_ERROR = 'admin-panel/form/SET_FIELD_ERROR';
export const TYPE_SET_ERROR = 'admin-panel/form/SET_ERROR';
export const TYPE_SET_IS_LOADING = 'admin-panel/form/SET_IS_LOADING';
export const TYPE_SET_IS_VALID = 'admin-panel/form/SET_IS_VALID';

export const initData = {
    fields: [],
    showFieldsErrors: false,
    showMainError: false,
    error: '',
    isLoading: false,
    isValid: false
};

export default function (state, action) {
    switch (action.type) {
        case TYPE_SET_FIELDS:
            return {...state, fields: action.payload};
        case TYPE_SET_FIELD_VALUE:
            return {
                ...state,
                fields: state.fields.map(item => {
                    if(item.name === action.payload.name) {
                        item.value = action.payload.value;
                        item.error = null;
                    }
                    return item;
                })
            };
        case TYPE_SET_FIELD_ERROR:
            return {
                ...state,
                fields: state.fields.map(item => {
                    if(item.name === action.payload.name)
                        item.error = action.payload.error;
                    return item;
                })
            };
        case TYPE_SET_ERROR:
            return {...state, error: action.payload};
        case TYPE_SET_IS_LOADING:
            return {...state, isLoading: action.payload};
        case TYPE_SET_IS_VALID:
            return {...state, isValid: action.payload};
        default:
            return state;
    }

}