import React from 'react';
import PropTypes from 'prop-types';

const Button = ({title, onClick, type, size, isOutline}) => {
    const classes = ['btn'];

    const outline = isOutline ? '-outline' : '';
    classes.push(`btn${outline}-${type}`);

    size && classes.push(`btn-${size}`);

    return (
        <button type="button" className={classes.join(' ')} onClick={onClick}>{title}</button>
    );
};

Button.propTypes = {
    title: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.oneOf(['primary', 'secondary', 'success', 'info', 'danger', 'warning']),
    size: PropTypes.oneOf(['lg', 'sm', 'xs']),
    isOutline: PropTypes.bool,
};


Button.defaultProps = {
    title: 'Button',
    onClick: () => console.log('Button click'),
    type: 'primary',
    isOutline: false
};
export default Button;