import React, {useState, useMemo} from 'react';
import PropTypes from 'prop-types';
import {useDropzone} from 'react-dropzone';

const Label = ({label, name, labelClass = ''}) => {
    return label ? <label htmlFor={`field-${name}`} className={labelClass}>{label}</label> : null;
};

const Text = props => {
    const {name, value: defaultValue, className, onChange, type} = props;
    const clearProps = {name, defaultValue, className, onChange, type};
    return (
        <>
            <Label {...props}/>
            <input {...clearProps} />
        </>
    )
};

const File = props => {
    const {name, value: defaultValue, className, onChange, type} = props;
    const clearProps = {name, defaultValue, className, onChange, type};

    const [file, setFile] = useState(defaultValue || 'Select file');
    const changeFile = (event) => {
        setFile(event.target.value.split(/(\\|\/)/g).pop() || 'Select file');
        props.onChange(event);
    };

    return (
        <>
            <Label label={props.label} name={name}/>
            <div className="input-group">
                <div className="custom-file">
                    <input {...clearProps}
                           className={clearProps.className + ' custom-file-input'}
                           onChange={changeFile}
                    />
                    <Label label={file} name={name} labelClass="custom-file-label"/>
                </div>
            </div>
        </>
    )
};

const Checkbox = props =>{
    const {name, className, onChange, type, id} = props;
    const clearProps = {name, className, onChange, type, id};

    return (
        <div className="custom-control custom-checkbox">
            <input {...clearProps}
                   className={clearProps.className + ' custom-control-input'}
            />
            <Label {...props} labelClass="custom-control-label"/>
        </div>
    );
};

const FileDropzone = props => {

    let [image, setImage] = useState('/default.jpg');

    const onDrop = files => {
        const reader = new FileReader();
        reader.onload = e => {
            setImage(e.target.result);
        };


        reader.readAsDataURL(files[0]);
        props.onChange(files[0]);
    };

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({accept: 'image/*', onDrop: onDrop});

    const baseStyle = {
        width: '100px',
        height: '100px',
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: '#bdbdbd',
        outline: 'none',
        transition: 'border .24s ease-in-out'
    };

    const activeStyle = {
        borderColor: '#2196f3'
    };

    const acceptStyle = {
        borderColor: '#00e676'
    };

    const rejectStyle = {
        borderColor: '#ff1744'
    };

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isDragActive,
        isDragReject
    ]);

    const allProps = Object.assign(getInputProps(), {name: props.name});

    return (
        <div className="container">
            <div {...getRootProps({style})}>
                <img src={image} alt="" style={{width: '100%', height: '100%'}}/>
                <input {...allProps} />
            </div>
        </div>
    );
};

const selectType = props => {
    switch (props.type) {
        case 'checkbox':
        case 'radio':
            return <Checkbox {...props}/>;
        case 'file':
            return <File {...props}/>;
        case 'dropzone':
            return <FileDropzone {...props}/>;
        default:
            return <Text {...props}/>;
    }
};
const Input = ({changeHandler, inputClass, value, wrapperClass, ...props}) => {
    const change = (event) => {
        let value = null;
        switch (props.type) {
            case 'checkbox':
            case 'radio':
                value = event.target.checked;
                break;
            case 'dropzone':
                value = event;
                break;
            default:
                value = event.target.value;
        }

        changeHandler(props.name, value);
    };
    return (
        <div className={`form-group ${wrapperClass}`}>
            {selectType({
                ...props,
                className: `form-control ${inputClass} ${props.error ? 'is-invalid' : ''}`,
                id: `field-${props.name}`,
                defaultValue: value,
                onChange: change
            })}
            {
                props.error ? <div className="invalid-feedback">{props.error}</div> : null
            }

        </div>
    );
};

Input.propTypes = {
    name: PropTypes.string,
    type: PropTypes.oneOf(['text', 'radio', 'email', 'checkbox', 'file', 'dropzone']),
    inputClass: PropTypes.string,
    wrapperClass: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    error: PropTypes.string
};

Input.defaultProps = {
    type: 'text',
    inputClass: '',
    wrapperClass: '',
    label: null,
    placeholder: '',
    error: null
};

export default Input;