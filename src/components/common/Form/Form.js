import React, {useReducer, useEffect} from 'react';
import PropTypes from 'prop-types';
import reducer, {
    initData,
    TYPE_SET_FIELD_ERROR,
    TYPE_SET_FIELD_VALUE,
    TYPE_SET_FIELDS,
    TYPE_SET_IS_VALID
} from './form.reducer';
import Input from "./Input";

const Form = ({schema, doSubmit, formRef}) => {
    const [formReducer, formDispatch] = useReducer(reducer, initData);

    useEffect(() => {
        formDispatch({
            type: TYPE_SET_FIELDS,
            payload: schema
        });
    }, []);

    const formSchema = formReducer.fields;

    const changeHandler = (name, value) => {
        formDispatch({
            type: TYPE_SET_FIELD_VALUE,
            payload: {name, value}
        });
    };

    const submitHandler = (event) => {
        event.preventDefault();
        const isValid = validate();
        const data = {};
        formReducer.fields.forEach(item => {
            data[item.name] = item.value;
        });

        doSubmit({formIsValid: isValid, data: data});
    };

    const validate = () => {
        let isValid = true;
        formReducer.fields.forEach(item => {
            if(!item.validation)
                return;
            const validator = item.validation(item.value);
            if(isValid && !validator.isValid)
                isValid = false;
            formDispatch({
                type: TYPE_SET_FIELD_ERROR,
                payload: {name: item.name, error: !validator.isValid ? validator.message : null}
            });
        });
        formDispatch({
            type: TYPE_SET_IS_VALID,
            payload: isValid
        });

        return isValid;
    };

    return (
        <form role="form" onSubmit={submitHandler}>
            {
                formSchema && formSchema.map(item =>
                    <Input key={item.name}
                           {...item}
                           changeHandler={changeHandler}
                    />)
            }
            <button type="submit" ref={formRef} className="d-none">Send</button>
        </form>
    );
};

Form.propTypes = {
    fields: PropTypes.array,
};

export default Form;