import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

const SidebarItem = ({title, url, icon, children}) => {

    const [isOpen, setOpen] = useState(false);

    const toggleMenu = (event) => {
        event.preventDefault();
        setOpen(!isOpen);
    };

    return (
        <li className={`nav-item ${children ? 'has-treeview' : ''} ${isOpen ? 'menu-open' : ''}`}>
            {
                !children ? (
                    <NavLink to={url} className="nav-link"> {icon} <p>{title}</p> </NavLink>
                ) : (
                    <>
                        <NavLink to={url} className='nav-link' onClick={(event) => toggleMenu(event)}>
                            {icon} <p>{title}<i className="right fas fa-angle-left" onClick={toggleMenu}/></p>
                        </NavLink>
                        <ul className="nav nav-treeview">
                            {children}
                        </ul>
                    </>
                )
            }
        </li>
    );
};

SidebarItem.propTypes = {
    title: PropTypes.string,
    icon: PropTypes.element,
    url: PropTypes.string,
};

SidebarItem.defaultProps = {
    title: 'Dashboard',
    icon: <i className="far fa-circle nav-icon"/>,
    url: ''
};

export default SidebarItem;