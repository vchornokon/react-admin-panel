import React, {useContext} from 'react';
import {ThemeContext} from '../../contexts/theme';
import {UserContext} from '../../contexts/user';
import SidebarItem from './SidebarItem';
import UsersSidebar from "../../pages/users/UsersSidebar";

const Sidebar = function () {
    const theme = useContext(ThemeContext);
    const {user} = useContext(UserContext);
    return (
        <aside className="main-sidebar sidebar-dark-primary elevation-4">
            <a href="../../index3.html" className="brand-link">
                <img src={theme.logo} alt="AdminLTE Logo" className="brand-image img-circle elevation-3"
                     style={{opacity: '.8'}}/>
                <span className="brand-text font-weight-light">AdminLTE 3</span>
            </a>
            <div className="sidebar">
                {
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        {
                            user &&
                            <>
                                <div className="image">
                                    <img src={user.avatar} className="img-circle elevation-2" alt="Avatar"/>
                                </div>
                                <div className="info">
                                    <a href="/" className="d-block">{user.name}</a>
                                </div>
                            </>
                        }
                    </div>
                }
                <nav className="mt-2">
                    <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <SidebarItem
                            title="Dashboard"
                            url="/app/dashboard"
                            icon={React.createElement('i', {className: 'nav-icon fas fa-tachometer-alt'})}
                        />
                        <UsersSidebar/>
                    </ul>
                </nav>
            </div>
        </aside>
    );
};

export default Sidebar;